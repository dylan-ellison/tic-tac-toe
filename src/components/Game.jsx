import React, { Component } from "react";
import Board from "./Board.jsx";

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      xIsNext: true,
      currentMove: 0,
    };
  }

  makeMove(i) {
    const history = this.state.history.slice(0, this.state.currentMove + 1);
    let squares = history[history.length -1].squares.slice();

    if (squares[i] || this.calculateWinner(squares)) {
      return;
    }
    squares[i] = this.state.xIsNext ? "X" : "O";

    this.setState({
      history: history.concat([{
        squares: squares
      }]),
      xIsNext: !this.state.xIsNext,
      currentMove: history.length
    });
  }

  calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 4, 8],
      [2, 4, 6],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8]
    ];

    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return squares[a];
      }
    }

    return null;
  }

  jumpTo(move) {
    this.setState({
      currentMove: move,
      xIsNext: move % 2 === 0 ? true : false
    })
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.currentMove];
    let squares = current.squares
    const winner = this.calculateWinner(squares);

    let status;
    if (winner) {
      status = `The winner is: ${winner}`;
    } else {
      status = `Next Move: ${this.state.xIsNext ? "X" : "O"}`;
    }
    return (
      <div className="game">
        <div>
          <div>{status}</div>
          <Board squares={squares} makeMove={i => this.makeMove(i)} />
        </div>
        <div>
          <ol>
          {history.map((squares, key) => {
            return (
              <li key={key}>
                <button onClick={() => this.jumpTo(key)}>
                  Go to move {key}
                </button>
              </li>
            )
          })}
          </ol>
        </div>
      </div>
    );
  }
}

export default Game;
